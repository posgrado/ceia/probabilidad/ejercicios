import numpy as np
from scipy import stats

# Número de muestras
n = 100

# Media muestral (cm)
x_ = 180

# Varianza de la población = np.square(sigma) (cm2)
varianza = 49
sigma = np.sqrt(varianza)

# Intervalo teórico
mu_min_teorico = x_ - 1.96 * (sigma / np.sqrt(n)) 
mu_max_teorico = x_ + 1.96 * (sigma / np.sqrt(n)) 
print(f'El intervalo de confianza teórico es ({mu_min_teorico}, {mu_max_teorico})')

x = np.random.normal(180, sigma / np.sqrt(100), 100)
print(f'El {100 * len(x[x < mu_max_teorico]) / len(x)}% de los valores están por debajo del mínimo teórico')
print(f'El {100 * len(x[x > mu_max_teorico]) / len(x)}% de los valores están por encima del máximo teórico')
print(f'El intervalo de confianza calculado es: {stats.norm.interval(0.95, x.mean(), x.std())}')
