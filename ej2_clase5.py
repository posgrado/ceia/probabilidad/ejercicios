import numpy as np
from scipy import stats

# Número de muestras
n = 10

# Media muestral (hs)
x_ = 998.9

# Varianza de la población = np.square(sigma) (hs)
# varianza = 49
sigma = 80

# Intervalo teórico
mu_min_teorico = x_ - 1.96 * (sigma / np.sqrt(n)) 
mu_max_teorico = x_ + 1.96 * (sigma / np.sqrt(n)) 
print(f'El intervalo de confianza teórico es ({mu_min_teorico}, {mu_max_teorico})')

x = np.random.normal(x_, sigma / np.sqrt(n), n)
print(f'El {100 * len(x[x < mu_min_teorico]) / len(x)}% de los valores están por debajo del mínimo teórico')
print(f'El {100 * len(x[x > mu_max_teorico]) / len(x)}% de los valores están por encima del máximo teórico')
print(f'El intervalo de confianza calculado es: {stats.norm.interval(0.95, x.mean(), x.std())}')

z = np.sqrt(10) / 2
interval = stats.norm.cdf(z) - stats.norm.cdf(-z)
print(interval)
